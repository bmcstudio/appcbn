(function(){

    'use strict';

    // app: CBN
    // developer by: Douglas de Oliveira

    /* jshint validthis: true */

    angular.module('AppCBN.filters', []);
    angular.module('AppCBN.services', []);
    angular.module('AppCBN.directives', []);
    angular.module('AppCBN.controllers', []);

    angular.module('AppCBN', ['ionic', 'AppCBN.httpInterceptor', 'LocalStorageModule', 'AppCBN.filters', 'AppCBN.services', 'AppCBN.directives', 'AppCBN.controllers'])

    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {

            if (typeof(navigator.notification) == "undefined") {
              navigator.notification = window;
            };
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
              cordova.plugins.Keyboard.disableScroll(true);
              cordova.plugins.Keyboard.disableScrollingInShrinkView(true);;
            }
            if (window.StatusBar) {
              // org.apache.cordova.statusbar required
              StatusBar.styleLightContent();
            }
        });
    })

    .config(function($stateProvider, $urlRouterProvider, $httpProvider, $ionicConfigProvider) {

        $stateProvider
          .state('home', {
              url: '/home',
              templateUrl: 'templates/home.html',
              controller: 'homeCtrl as vm'
          })
          .state('result', {
              url: '/result',
              templateUrl: 'templates/result.html',
              controller: 'resultCtrl as vm'
          })
          .state('login', {
              url: '/login',
              templateUrl: 'templates/login.html',
              controller: 'loginCtrl as vm'
          })
          .state('video', {
              url: '/video',
              templateUrl: 'templates/video.html'
          });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/home');
        // Add the interceptor to the $httpProvider.
        $httpProvider.interceptors.push('httpInterceptor');

        $ionicConfigProvider.backButton.previousTitleText(false).text('');
    });

})();