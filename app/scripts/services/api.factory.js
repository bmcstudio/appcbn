(function() {

	'use strict';

	/* jshint validthis: true */

	angular.module('AppCBN.services')
		.factory('$api', api);

	api.$inject = ['$http'];

	function api($http) {

		var urlBase = 'http://bmc.ppg.br/bardez/sbnapp/',
			api = {
				login     : login,
				getSearch : getSearch,
				getResult : getResult
			};

		return api;

		///////////////////////////////////////


		function login (argument) {
			return $http.get( urlBase + 'verifyPass.php', { params: argument} );
		}

		function getSearch(options) {
			return $http.get( urlBase + 'getSelects.php', { params: options} );
		}

		function getResult(options) {
			return $http.get( urlBase + 'getResults.php', { params: options} );
		}
	}

})();