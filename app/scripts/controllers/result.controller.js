(function(){

	'use strict';

	/* jshint validthis: true */

	angular.module('AppCBN.controllers')
		.controller('resultCtrl', resultCtrl);

	resultCtrl.$inject = ['$api', '$ionicLoading', '$rootScope', '$state', '$sce', 'localStorageService'];

	function resultCtrl($api, $ionicLoading, $rootScope, $state, $sce, localStorageService) {

		var vm = this;

		vm.results = [];
		vm.goVideo = goVideo;

		///////////////////////

		$ionicLoading.show({
			template: ' <div> \
							<i class="icon ion-loading-c"></i> \
							<p>Verificando</p> \
						</div>'
		});

		$api.getResult($rootScope.options).then(doneCallbacks, failCallbacks);

		function doneCallbacks(response) {
			// console.log(response);
			$ionicLoading.hide();
			vm.results = response.data;
		}

		function failCallbacks(error) {
			console.log(error);
			navigator.notification.alert(
				'Não foi possível estabelecer conexão com o servidor. Por favor tente novamente mais tarde.',  // message
				null,         // callback
				'Atenção',    // title
				'OK'          // buttonName
			);
		}

		function goVideo(link) {
			if (localStorageService.get('user') == null)
				$state.go('login');
			else{
				// console.log(link + '01_player.html');
				$rootScope.linkVideo = $sce.trustAsResourceUrl(link + '01.mp4');
				$state.go('video');
			}
		}

	}

})();