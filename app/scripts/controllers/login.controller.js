(function(){

	'use strict';

	/* jshint validthis: true */

	angular.module('AppCBN.controllers')
		.controller('loginCtrl', loginCtrl);

	loginCtrl.$inject = ['$api', '$rootScope', '$ionicLoading', '$ionicHistory', 'localStorageService'];

	function loginCtrl($api, $rootScope, $ionicLoading, $ionicHistory, localStorageService) {

		var vm = this;

		vm.loginData        = {};
		vm.combo = [
			{value: 0, txt: 'Associado'},
			{value: 1, txt: 'Sociedades Internacionais'},
			{value: 2, txt: 'Médicos Internacionais'},
			{value: 3, txt: 'Estudante de medicina'},
			{value: 4, txt: 'Laboratórios'}
		];
		vm.doLogin          = doLogin;
		vm.openExternalLink = openExternalLink;

		///////////////////////


		function doLogin() {
			$ionicLoading.show({
				template: ' <div> \
								<i class="icon ion-loading-c"></i> \
								<p>Verificando</p> \
							</div>'
			});
			$api.login(vm.loginData).then(doneLogin, failLogin);
		}

		function doneLogin(response) {
			$ionicLoading.hide();
			localStorageService.set('user', response.data);
			$rootScope.isLogged = true;
			$ionicHistory.goBack();
		}

		function failLogin(response) {
			$ionicLoading.hide();
			navigator.notification.alert(
				response.data,  // message
				null,         	// callback
				'Atenção',    	// title
				'OK'          	// buttonName
			);
		}

		function openExternalLink() {
			// window.open('http://www.xrleiloes.com.br/leilao/registration.html', '_system', 'location=no');
		}

	}

})();