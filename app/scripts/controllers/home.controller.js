(function(){

	'use strict';

	/* jshint validthis: true */

	angular.module('AppCBN.controllers')
		.controller('homeCtrl', homeCtrl);

	homeCtrl.$inject = ['$api', '$rootScope', '$state', '$ionicLoading', 'localStorageService'];

	function homeCtrl($api, $rootScope, $state, $ionicLoading, localStorageService) {

		var vm = this;

		vm.searchs = [];
		$rootScope.isLogged = (localStorageService.get('user') != null) ? true : false;

		vm.filterSearch = filterSearch;
		vm.goResults    = goResults;
		vm.clearSearch  = clearSearch;
		vm.loginlogout  = loginlogout;

		///////////////////////

		clearSearch();

		function doneCallbacks(response) {
			// console.log(response);
			$ionicLoading.hide();
			vm.searchs = response.data;
		}

		function failCallbacks(error) {
			// console.log(error);
			navigator.notification.alert(
				'Não foi possível estabelecer conexão com o servidor. Por favor tente novamente mais tarde.',  // message
				null,         // callback
				'Atenção',    // title
				'OK'          // buttonName
			);
		}

		function filterSearch() {
			$ionicLoading.show({
				template: ' <div> \
								<i class="icon ion-loading-c"></i> \
								<p>Verificando</p> \
							</div>'
			});
			$api.getSearch($rootScope.options).then(doneCallbacks, failCallbacks);
		}

		function goResults() {
			$state.go('result');
		}

		function clearSearch() {
			$ionicLoading.show({
				template: ' <div> \
								<i class="icon ion-loading-c"></i> \
								<p>Verificando</p> \
							</div>'
			});
			$rootScope.options = {
				palavras     : '',
				modulo       : 0,
				tema         : 0,
				palestras    : 0,
				palestrantes : 0,
				datas        : 0
			};
			$api.getSearch().then(doneCallbacks, failCallbacks);
		}

		function loginlogout() {
			if (localStorageService.get('user') == null)
				$state.go('login');
			else{
				localStorageService.clearAll();
				$rootScope.isLogged = false;
			}
		}

	}

})();